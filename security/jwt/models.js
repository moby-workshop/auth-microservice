class JwtPayload {
    constructor(id) {
        this.userId = id;
    }

    toPlainObject (){
        return {
            userId: this.userId
        }
    }
}

module.exports = {
    JwtPayload
}