const {
    verifyAndDecodeData
} = require('./token.js');

const {
    ServerError
} = require('../../errors');


const verifyAuthHeader = (req) => {
    if (!req.headers.authorization) {
        throw new ServerError('Authorization Header missing!', 401);
    }
}

const authorizeAndExtractToken = async (req, res, next) => {
    try {
        verifyAuthHeader(req);
        const token = req.headers.authorization.split(" ")[1];

        const decoded = await verifyAndDecodeData(token);
        req.state = {
            decoded
        };

        next();
    } catch (err) {
        next(err);
    }
};

const authorizeAndExtractTokenNoExpire = async (req, res, next) => {
    try {
        verifyAuthHeader(req);
        const token = req.headers.authorization.split(" ")[1];

        const decoded = await verifyAndDecodeData(token, true);
        req.state = {
            decoded
        };
        
        next();
    } catch (err) {
        next(err);
    }
};

module.exports = {
    authorizeAndExtractToken,
    authorizeAndExtractTokenNoExpire
}

